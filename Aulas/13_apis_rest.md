# APIs e Banco de Dados

> Objetivo: Construir APIs REST com express e conecta-la a um Banco de Dados MongoDB.

## O que é API?

Uma API (Application Programming Interface) é um conjunto de características e regras existentes em uma aplicação que possibilitam interações com essa através de um software - ao contrário de uma interface de usuário. A API pode ser entendida como uma simples comunicação - *intermediada por ela mesma* - entre a aplicação que a fornece e outros itens, como outros componentes do software, ou software de terceiros.

No desenvolvimento Web, uma API é geralmente um conjunto de métodos padronizados, properties, eventos, e URLs que um desenvolvedor pode usar em seus aplicativos para interagir com componentes desenvolvidos para um usuário ou outro software/hardware ou sites e serviços de terceiros.

## Representação de APIs

Agora que já sabemos que uma API permite a interoperabilidade entre usuários e aplicações, isso reforça ainda mais a importância de pensarmos em algo padronizado e, de preferência, de fácil representação e compreensão por humanos e máquinas. Isso pode soar um pouco estranho, mas veja esses três exemplos:

### Representação XML
```xml	
<!-- XML é uma linguagem de marcação de documentos --> 

<endereco>
  <rua>
    Rua Recife
  </rua>
  <cidade>
    Paulo Afonso
  </cidade>
</endereco>
```

### Representação JSON
```jsonc	
// JSON é uma linguagem de representação de dados em objetos javascript

{ endereco:
  {
  rua: Rua Recife,
  cidade: Paulo Afonso
  }
}
```

### Representação YAML
```yaml	
# YAML é uma linguagem de representação de dados para facilitar a leitura por humanos

endereco:
  rua: rua Recife
  cidade: Paulo Afonso
```

Contudo, as 3 representações são válidas, pois nosso entendimento final é o mesmo, ou seja, a semântica é a mesma.

## Métodos de requisição HTTP (Verbos)

O protocolo HTTP define um conjunto de métodos de requisição responsáveis por indicar a ação a ser executada para um dado recurso. Embora esses métodos possam ser descritos como substantivos, eles também são comumente referenciados como HTTP Verbs (Verbos HTTP). Cada um deles implementa uma semântica diferente, mas alguns recursos são compartilhados por um grupo deles, como por exemplo, qualquer método de requisição pode ser do tipo `safe`, `idempotent` ou `cacheable`. Alguns exemplos de métodos de requisição HTTP são:

### `GET`
O método GET solicita a representação de um recurso específico. Requisições utilizando o método GET devem retornar apenas dados.

### `POST`
O método POST é utilizado para submeter uma entidade a um recurso específico, frequentemente causando uma mudança no estado do recurso ou efeitos colaterais no servidor.

### `PUT`
O método PUT substitui todas as atuais representações do recurso de destino pela carga de dados da requisição.

### `DELETE`
O método DELETE remove um recurso específico.

### `PATCH`
O método PATCH é utilizado para aplicar modificações parciais em um recurso.

Existem outros métodos pouco usados no desenvolvimento web mas existem para o protocolo, como HEAD, OPTIONS, etc.

## Criação de APIs com Express

O Express é um framework para aplicativo da web do Node.js mínimo e flexível que fornece um conjunto robusto de recursos para aplicativos web e móvel.

https://expressjs.com/pt-br/

Você também possui o Gerador de Arquivos do Express para te ajudar a criar o seus diretórios, pense nele como um `create-react-app` do back-end.

https://expressjs.com/pt-br/starter/generator.html

### Roteamento

O Roteamento refere-se à definição de terminais do aplicativo (URIs) e como eles respondem às solicitações do cliente. Para obter uma introdução a roteamento, consulte [Roteamento básico](https://expressjs.com/pt-br/starter/basic-routing.html).

O código a seguir é um exemplo de uma rota muito básica.

``` javascript
var express = require('express');
var app = express();

// respond with "hello world" when a GET request is made to the homepage
app.get('/', function(req, res) {
  res.send('hello world');
});
```

### Métodos de roteamento

Um método de roteamento é derivado a partir de um dos métodos HTTP, e é anexado a uma instância da classe express.

O código a seguir é um exemplo de rotas para a raiz do aplicativo que estão definidas para os métodos GET, POST, PUT e DELETE.

``` javascript
// GET method route
app.get('/', function (req, res) {
  res.send('GET request to the homepage');
});

// POST method route
app.post('/', function (req, res) {
  res.send('POST request to the homepage');
});

// PUT method route
app.put('/user', function (req, res) {
  res.send('Got a PUT request at /user');
});

// DELETE method route
app.delete('/user', function (req, res) {
  res.send('Got a DELETE request at /user');
});
```

- Para saber mais sobre os métodos de requisição HTTP: https://developer.mozilla.org/pt-BR/docs/Web/HTTP/Methods
- Para mais informações sobre os status de requisição HTTP: https://httpstat.us/
- Referência da API **Express JS**: https://expressjs.com/pt-br/4x/api.html

## Cabeçalhos HTTP (Headers)

Os cabeçalhos HTTP permitem que o cliente e o servidor passem informações adicionais com a solicitação ou a resposta HTTP. Um cabeçalho de solicitação é composto por seu nome case-insensitive (não diferencia letras maiúsculas e minúsculas), seguido por dois pontos ':' e pelo seu valor (sem quebras de linha).  Espaços em branco antes do valor serão ignorados.

Alguns exemplos:

- **Cache-Control:** Especifica diretivas para mecanismos de cache em requisições e respostas.
- **Expires:** A data/hora depois que a resposta é considerada obsoleta.
- **Connection**: Controla se uma conexão de rede continua ou não aberta após o término da transação atual.
- **Keep-Alive**: Controla por quanto tempo uma conexão persistente deve permanecer aberta.
- **Accept**: Informa ao servidor sobre os tipos de dados que podem ser enviados de volta. Isto é MIME-type.
- **Accept-Charset**: Informa ao servidor sobre qual conjunto de caracter o cliente é capaz de entender.
- **Cookie:** Contém cookies HTTP armazenados préviamente enviados pelo servidor com o cabeçalho `Set-Cookie`.
- **Set-Cookie:** Envia cookies do servidor para o agente de usuário.
- **Access-Control-Allow-Origin**: Indica se a resposta pode ser compartilhada.
- **Content-Disposition:** Em uma resposta HTTP regular, é um cabeçalho que indica se o conteúdo deve ser exibido em linha no navegador, ou seja, como uma página da Web ou como parte de uma página da Web ou como um anexo, que é baixado e salvo localmente.
- **Content-Type:** Indica o tipo de mídia do recurso.
- **Content-Encoding:** Usado para especificar o algoritmo de compressão.

## Usando Middlewares com Express

O Express é uma estrutura web de roteamento e middlewares que tem uma funcionalidade mínima por si só: Um aplicativo do Express é essencialmente uma série de chamadas de funções de middleware.

Funções de Middleware são funções que tem acesso ao objeto de solicitação (`req`), o objeto de resposta (`res`), e a próxima função de middleware no ciclo solicitação-resposta do aplicativo. A próxima função middleware é comumente encerrada por uma variável chamada `next`.

Funções de middleware podem executar as seguintes tarefas:
- Executar qualquer código.
- Fazer mudanças nos objetos de solicitação e resposta.
- Encerrar o ciclo de solicitação-resposta.
- Chamar a próxima função de middleware na pilha.

Se a atual função de middleware não terminar o ciclo de requisição-resposta, ela precisa chamar `next()` para passar o controle para a próxima função de middleware. Caso contrário, a solicitação ficará suspensa.

Um aplicativo Express pode usar diferentes tipos de middlewares, porém iremos focar no `Middleware de nível do aplicativo`, vinculando middlewares de nível do aplicativo a uma instância do objeto `app` usando a função `app.use()`:

```js
const app = express();

app.use((req, res, next) => {
  console.log('Time:', Date.now());
  next();
});
```

Podemos montar middlewares em uma rota:

```js
app.use('/user/:id', (req, res, next) => {
  console.log('id param:', req.params.id);
  next();
});
```
Neste caso, `:id` é um parâmetro dinâmico que pode ser acessado via `req.params.id`.


## Conectando a API ao Banco de Dados

## Utilizando o Mongoose

O Mongoose fornece uma solução direta e baseada em esquema para modelar os dados do seu aplicativo. Inclui conversão de tipo embutida, validação, construção de consulta, ganchos de lógica de negócios e muito mais, prontos para uso.

Primeiro, certifique-se de ter MongoDB e Node.js instalados.

Em seguida, instale o Mongoose a partir da linha de comando usando npm:

```
$ npm install mongoose --save
```

Agora digamos que gostamos de gatinhos peludos e queremos registrar todos os gatinhos que conhecemos no MongoDB. A primeira coisa que precisamos fazer é incluir o `mongoose` em nosso projeto e abrir uma conexão com o banco de dados em nossa instância do MongoDB em execução local.

```js
// getting-started.js
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true});
```

Temos uma conexão com o banco de dados de teste em execução no localhost. Agora precisamos ser notificados se nos conectamos com sucesso ou se ocorrer um erro de conexão:

```js
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
});
```

Assim que nossa conexão for aberta, nosso retorno de chamada será realizado. Para resumir, vamos supor que todo o código a seguir esteja dentro desse retorno de chamada.

Com o Mongoose, tudo é derivado de um `Schema` . Vamos obter uma referência a ele.

```js
const kittySchema = new mongoose.Schema({
  name: String
});
```

Até agora tudo bem. Temos um esquema com uma propriedade `name`, que será a _String_. A próxima etapa é compilar nosso esquema em um modelo .

```js
const Kitten = mongoose.model('Kitten', kittySchema);
```

Um `Model` é uma classe com a qual construímos documentos. Nesse caso, cada documento será um gatinho com propriedades e comportamentos conforme declarado em nosso esquema. Vamos criar um documento de gatinho representando o garotinho que acabamos de conhecer na calçada do lado de fora:

```js
const silence = new Kitten({ name: 'Silence' });
console.log(silence.name); // 'Silence'
```

Os gatinhos podem miar, então vamos dar uma olhada em como adicionar a funcionalidade de "fala" aos nossos documentos:

```js
// NOTE: methods must be added to the schema before compiling it with mongoose.model()
kittySchema.methods.speak = function () {
  const greeting = this.name
    ? "Meow name is " + this.name
    : "I don't have a name";
  console.log(greeting);
}

const Kitten = mongoose.model('Kitten', kittySchema);
```

As funções adicionadas à propriedade de um esquema são compiladas no Model protótipo e expostas em cada instância do documento:

```js
const fluffy = new Kitten({ name: 'fluffy' });
fluffy.speak(); // "Meow name is fluffy"
```

Mas ainda não salvamos nada no MongoDB. Cada documento pode ser salvo no banco de dados chamando seu método de salvamento. O primeiro argumento para o retorno de chamada será um erro, se ocorrer.

```js
fluffy.save(function (err, fluffy) {
  if (err) return console.error(err);
  fluffy.speak();
});
```

Diga que o tempo passa e queremos mostrar todos os gatinhos que vimos. Podemos acessar todos os documentos de gatinhos por meio de nosso modelo Kitten .

```js
Kitten.find(function (err, kittens) {
  if (err) return console.error(err);
  console.log(kittens);
})
```

Acabamos de registrar todos os gatinhos em nosso banco de dados no console. Se quisermos filtrar nossos gatinhos por nome, o Mongoose oferece suporte à sintaxe de consulta avançada do MongoDB .

```js
Kitten.find({ name: /^fluff/ }, callback);
```

Isso executa uma pesquisa por todos os documentos com uma propriedade de nome que começa com "fluff" e retorna o resultado como uma matriz de gatinhos para o retorno de chamada.


## Docs:

- [Cabeçalhos HTTP](https://developer.mozilla.org/pt-BR/docs/Web/HTTP/Headers)
- [Entendendo um pouco mais sobre o protocolo HTTP](https://nandovieira.com.br/entendendo-um-pouco-mais-sobre-o-protocolo-http)
- [Usando middlewares](https://expressjs.com/pt-br/guide/using-middleware.html)
- [Escrevendo middlewares pra uso em aplicativos do Express](https://expressjs.com/pt-br/guide/writing-middleware.html)
- [Entendendo como funciona os middlewares do Express](https://udgwebdev.com/entendendo-como-funciona-os-middlewares-do-express/)
- [express async await](https://lombardo-chcg.github.io/languages/2017/09/18/express-async-await.html)
- [Tutorial Mongoose](https://developer.mozilla.org/pt-BR/docs/Learn/Server-side/Express_Nodejs/mongoose)
- [Conheça as Queries do Mongoose](https://mongoosejs.com/docs/queries.html)
- [Criando um CRUD completo com NodeJS, Express e MongoDB](https://medium.com/baixada-nerd/criando-um-crud-completo-com-nodejs-express-e-mongodb-parte-1-3-6c8389d7147d)
- [Criando uma API REST com Node.js + Express + Mongoose](https://medium.com/@rafaelbarbosadc/criando-uma-api-rest-com-node-js-express-mongoose-f75a27e8cdc1)
- [Import and Export Data](https://docs.mongodb.com/compass/master/import-export)
- [Mongoose: How to Import and Export Data](https://mongoosejs.com/docs/import-export.html)
- [Restful API with Node.js, Express, MongoDB and Mongoose](https://medium.com/@rafaelbarbosadc/restful-api-with-node-js-express-mongodb-and-mongoose-d0f8f8f8f8f8)
- [REST vs RESTful](https://blog.ndepend.com/rest-vs-restful/)
- [Params, Query Params e Json Express.js](https://medium.com/@kaiquecovo/params-query-params-e-json-express-js-7d174703a6ef)

## Desafio

Neste desfafio vocês devem criar endpoints que conversem com o banco de dados, ou seja, criar endpoints com o Express que se comunique com o MongoDB.

### Banco de Dados

O banco deverá ter duas entidades: **Aluno**, **Curso**

**Aluno**
- id: string
- nome: string
- sobrenome: string
- idade: number
- idCurso: string (Regra: Um aluno só pode ter um curso)

**Curso**
- id: string
- nome: string
- diasDaSemana: enum (Regra: Sábado e Domingo devem dar erro e apresentar uma mensagem de erro)
  - Segunda
  - Terça
  - Quarta
  - Quinta
  - Sexta

### Endpoints

- Busca de Alunos (mais de um tipo de busca não pode acontecer ao mesmo tempo)
  - Por ID
  - Por nome e sobrenome _*_
  - Por ID do curso _*_

- Busca de Curso (mais de um tipo de busca não pode acontecer ao mesmo tempo)
  - Por ID
  - Por dia da semana _*_

- Cadastro de Alunos
- Cadastro de Cursos

- Alterar aluno de curso (enviar os IDs dos registros)
- Alterar curso para outro dia da semana (enviar os IDs dos registros) _*_

***Para se destacar***

Para a entrega, crie um banco no **Mongodb Atlas** e conectem à sua aplicação ou **exportem** o banco de dados **já populado** para podermos realizar testes mais facilmente e coloquem no repositório.

### Inicialização:

- De um Fork neste repositório;
- Agora você deve clonar o repositório em sua máquina, há um botão azul "Clone" no seu repositório do GitLab, clique nele e use a URL com HTTPS: `git clone url-copiada`;
- Agora dentro da pasta clonada, clique com o botão direito do mouse e clique em "Git Bash Here" para entrar na pasta pelo terminal;

### Entrega:

- Crie uma branch `seu-nome/api-mongodb`;
- Faça commits;
- Assim que terminar dê `git push origin seu-nome/api-mongodb`, crie um Merge Request na interface do GitLab, acesse o menu "Merge Requests" e crie um, configure o "Target Branch" para o repositório original para que seu App seja avaliado e revisado e para que possamos te dar um feedback;
- O nome do Merge Request deve ser o seu nome completo.