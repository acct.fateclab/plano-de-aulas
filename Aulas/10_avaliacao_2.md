# Avaliação II

Nesta segunda avaliação, vocês precisarão criar um carrinho de compras, onde o usuário poderá adicionar produtos e vizualizar os itens no carrinho com a soma das quantidades e preço, possibilitando colocar um desconto sobre o total do pedido.

## Data da entrega:
- **24/05/2022**

## Comportamento da aplicação:
- Poderá adicionar um produto com preço e quantidade, através de um form onde você irá criar as informações do mesmo. 
- Ao clicar em um botão de "Adicionar ao carrinho", o produto dever ser adicionado e exibido no carrinho.
- Ao clicar no botão de "Remover" em um produto do carrinho, o mesmo dever ser removido da lista e o total do carrinho ser recalculado
- Não poderá ser permitido duplicação de produtos no carrinho, mas ao invés disso, atualizar a quantidade com +1.
- Deverá ser o mais funcional possível, com comportamento de carrinho de compra


## Orientações:
- Inicialmente deverá ter um formulário para colocar as informações do produto que irá para o carrinho
- Poderá usar a mesma página para exibir o carrnho ou poderá criar os componentes em views separadas
- É necessário fazer uso de props e eventos para comunicar os componentes
- Faça uso do `useContext` para facilitar a comunicações do estado entre componentes e obter o total do carrinho

## Dicas:
- Procure refatorar o código usando a **Composição**.
- Use [Window.localStorage](https://developer.mozilla.org/pt-BR/docs/Web/API/Window/localStorage) para armazenar uma "cache" do carrinho. O `localStorage` pode ser utilizado para o usuário não perder os produtos em um reload da página.

## Para se Destacar:
- Crie uma aplicação **responsiva**.
- Utilize os conceitos do **PWA**.
- Utilize TypeScript.
- Utilize Gatsby.

## Inicialização:
- De um Fork neste repositório;
- Clonar o repositório localmente, há um botão azul "Clone" no seu repositório do GitLab, clique nele e use a URL com HTTPS, ou SSH se você já configurou uma [chave SSH](https://docs.gitlab.com/ee/ssh/).
- Agora localmente abra uma pasta e use o botão direito do Mouse para abrir o "Git Bash", com esse atalho você chegará na pasta que quer mais rapidamente pelo terminal.
- Use o comando git clone `url-copiada-do-gitlab` para que a estrutura de pastas do repositório seja clonada na sua pasta.
- Crie uma branch `seu-nome/avaliacao-2`.

## Entrega:
- Assim que terminar dê `git push -u origin nome-da-sua-branch` para o primeiro `push`. Demais `pushs` basta utilizar apenas `git push`.
- Acesse o menu "Merge Requests", configure o "Target Branch" para o repositório original para que seu App seja avaliado e revisado e para que possamos te dar um feedback.
- O nome do Merge Request deve ser o seu nome completo.
- Crie o Merge Request.

