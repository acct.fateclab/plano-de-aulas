# 09 - Introdução ao Gatsby

> **Objetivo**: Criar o setup inicial para um site Gatsby

## O que é Gatsby

[Gatsby](https://www.gatsbyjs.com/) é uma estrutura React que permite criar aplicativos estáticos e sem servidor. Os sites da Gatsby são diferentes dos sites tradicionais porque geralmente são implantados em uma rede de distribuição de conteúdo (CDN) e são independentes de conteúdo. A vantagem de implantar a partir de um CDN é que há menos latência e os sites geralmente são servidos ao cliente com mais rapidez.

Gatsby é frequentemente descrito como uma malha de conteúdo. Uma malha de conteúdo significa que, como usuário, você é capaz de extrair dados de diversas fontes, como um site WordPress , um arquivo CSV e uma variedade de APIs externas; como resultado, Gatsby é agnóstico em relação aos dados.

Esses sites sem servidor também são conhecidos como JAMStack, que significa JavaScript, API e Markup. Em sites Jamstack, a lógica do aplicativo normalmente reside no lado do cliente, sem ser fortemente acoplada ao servidor backend.

## Instalação

## Pré-requisitos

- [Node.js](https://nodejs.org/) 14.15.0 ou mais recente.

### Gatsby CLI

Instale o [Gatsby CLI](https://www.gatsbyjs.com/docs/reference/gatsby-cli/), globalmente (`-g`), por linha de comando. Esta interface de linha de comando Gatsby permitirá que você crie e personalize um novo site:

```shell
npm install -g gatsby-cli
```

Execute `gatsby help` para encontrar comandos úteis que poderam ser usados na criação de um novo site:

```shell
gatsby help
```

Comandos importantes:

- `gatsby new`: cria um novo site.
- `gatsby develop`: inicia o servidor de desenvolvimento.
- `gatsby build`: agrupa arquivos estáticos e ativos e cria uma versão de produção de seu aplicativo.

Uma das vantagens do Gatsby é que você não precisa codificar um site do zero. Gatsby tem vários modelos iniciais que você pode usar para colocar seu site em funcionamento.

Vamos usar o [Gatsby Starter Default](https://github.com/gatsbyjs/gatsby-starter-default), um dos modelos iniciais oficiais de Gatsby. Para isso, crie um novo site com:

```shell
gatsby new gatsby-starter-default https://github.com/gatsbyjs/gatsby-starter-default
```

### Arquivos e pastas importantes:

- `gatsby-config.js`: Contém as personalizações em todo o site. É aqui que você modificará os metadados e adicionará os [plugins Gatsby](https://www.gatsbyjs.com/docs/plugins/).
- `src`: O diretório contém todas as páginas, imagens e componentes que compõem o site.

### Configuração inicial:

Abra o arquivo `gatsby-config.js` para alterar o título, descrição e os metadados do autor:

```js
module.exports = {
  siteMetadata: {
    title: `Introdução ao Gatsby`,
    description: `Como criar um site em Gatsby`,
    author: `Fatec Lab`,
  },
  // ...
}
```

### Iniciando o desenvolvimento:

Execute o seguinte comando em seu terminal:

```shell
gatsby develop
```

Acesse `http://localhost:8000` para acessar o site local.

Abra o arquivo `src/pages/index.js` para começar seu novo site em Gatsby!

## Documentações:

- [Tutorial: Learn how Gatsby works](https://www.gatsbyjs.com/docs/tutorial/)
- [Adding Typescript to Gatsby](https://medium.com/@whoisryosuke/adding-typescript-to-gatsby-c4a8cdcb0e7e)

## Exercício

Neste exercício, você deverá refazer o exercício anterior usando Gatsby. Você poderá incrementá-lo ou ainda criar uma nova aplicação de mesma complexidade.

### Para se destacar:

- Use TypeScript.

### Entrega:

- Faça o Fork deste repositório.
- Crie uma `branch` chamada `seu-nome/introducao-ao-gatsby`
- Assim que terminar dê `git push origin seu-nome/introducao-ao-gatsby`.
- Acesse o menu "Merge Requests", configure o "Target Branch" para o repositório original para que seu App seja avaliado e revisado e para que possamos te dar um feedback.
- O nome do Merge Request deverá conter seu nome completo.
- Crie o Merge Request.
s- Envie o Merge Request.