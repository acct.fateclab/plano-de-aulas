# Informações sobre o Curso

Olá Aluno :smiley:!

Este curso possui o objetivo de contemplarmos os conceitos básicos das principais tecnologias usadas para o desenvolvimento web atual.

Além disso iremos trabalhar com tecnologias _open-source_, a fim de inserir o aluno dentro desse universo.

Dentro dele, iremos cobrir as seguintes tecnologias:

- **Git**: é um sistema de controle de versões distribuído, usado principalmente no desenvolvimento de software, mas pode ser usado para registrar o histórico de edições de qualquer tipo.
- **Javascript**: é uma linguagem de programação interpretada estruturada, de script em alto nível com tipagem dinâmica e multiparadigma. Juntamente com HTML e CSS, o JavaScript é uma das três principais tecnologias da World Wide Web.
- **React**: Biblioteca para construção de Interfaces de Usuário (UI) criada pelo Facebook.
- **Typescript**: É um superconjunto de JavaScript desenvolvido pela Microsoft que adiciona tipagem e alguns outros recursos a linguagem.
- **Gatsby** - Gatsby é um gerador de site estático de código aberto construído sobre Node.js usando React e GraphQL. Ele fornece mais de 2500 plugins para criar sites estáticos com base em fontes como documentos Markdown, MDX, imagens, vários sistemas de gerenciamento de conteúdo e muito mais.
- **NodeJS** - Ambiente de desenvolvimento backend para construções de APIs modernas usando a linguagem JavaScript.
- **ExpressJS** - Framework JavaScript para construção de APIs RESTful.
- **MongoDB** - Banco de dados NOSQL escalável usado para persistência de dados.
- **Mongoose** - Biblioteca para o MongoDB que facilita a criação de modelos para o MongoDB.
- **GraphQL** - É uma linguagem de consulta usada para construção de APIs.

## Instrutores

- ***Gabriel***: gabriel.carvalho@acct.global
- ***Rodrigo***: rodrigo.alves@acct.global

## Slack
Disponibilizaremos um canal de comunicação online que poderá ser utilizado pelos alunos para **tirar dúvidas em qualquer dia e horário da semana**. A resposta será disponibilizada em até 1 dia útil.

Dentro do Slack teremos os canais por tema (_#react, #git, #javascript, #nodejs, #mongodb_) e o canal de anúncios gerais (_#general_).

Sempre que disponibilizarmos uma nova aula, faremos o anúncio notificando no canal #general do Slack.

**Link de Acesso**: [https://join.slack.com/t/acctlab/shared_invite/zt-7ch8n1sy-FlCOrJkbGoVGPvBcotdpWw](https://join.slack.com/t/acctlab/shared_invite/zt-7ch8n1sy-FlCOrJkbGoVGPvBcotdpWw)

![Slack Link](https://i.imgur.com/8udnNqJ.png)

## Programação do Curso

### Duração:
- Duração total do curso de 4 meses 
- Total de dedicação mínima semanal: 2 horas semanais de aula online.

### Primeira fase - FRONT-END:

#### Fundamentos de Javascript e aplicações web front-end 
- Semana 1 - Introdução ao Javascript e ao GIT
- Semana 2 - Aplicações mais avançadas em Javascript e GIT

#### Construção de aplicações web front-end com React, com ênfase em Progressive Web Apps (Google) e geração de sites estáticos com Gatsby
- Semana 3 - Introdução ao React
- Semana 4 - Uso de Components, Props, State and  Conditional Rendering.
- Semana 5 - React Hooks
- Semana 6 - Conceitos de PWA, Network, Requisições e `fetch`
- Semana 7 - React com Typescript
- Semana 8 - Geração de sites estáticos com Gatsby

### Segunda fase - BACK-END:

#### Construções de aplicações web back-end e API's com NODEJS e Persistência de dados em banco de dados NOSQL com MongoDB 
- Semana 9 - Introdução ao Node.js
- Semana 10 - Introdução ao MongoDB
- Semana 11 - Utilizando APIs com express consumindo o banco de dados
- Semana 12 - Introdução ao graphql


#### Projeto Final 
- Semana 13 - Inciando o Projeto final
- Semana 14 - Entrega do Front-end e checkpoint do projeto
- Semana 15 - Entrega do Projeto (Back-end e Front-end)
