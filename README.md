# Plano de Aulas - Laboratório de Desenvolvimento Web FATEC

## Cronograma de aulas 

- [00 - Introdução ao Curso](./Aulas/00_introducao_curso.md)
- [01 - Introdução ao Javascript e Git](./Aulas/01_introducao_javascript.md)
- [02 - Introdução ao Javascript e Git II](./Aulas/02_introducao_javascript_ii.md)
- [03 - Introdução ao React](./Aulas/03_introducao_react.md)
- [04 - React Componentes e Props](./Aulas/04_react_componentes_props.md)
- [05 - React Hooks](./Aulas/05_react_hooks.md)
- [06 - Avaliação 1](./Aulas/06_avaliacao_1.md)
- [07 - PWA e Requisições HTTP](./Aulas/07_pwa_requisicoes_http.md)
- [08 - React com Typescript](./Aulas/08_react_typescript.md)
- [09 - Introdução ao Gatsby](./Aulas/09_introducao_gatsby.md)
- [10 - Avaliação 2](./Aulas/10_avaliacao_2.md)
- [11 - Introdução ao NodeJS](./Aulas/11_introducao_nodejs.md)
- [12 - Introdução ao MongoDB](./Aulas/12_introducao_mongodb.md)
- [13 - APIs REST e Banco de Dados](./Aulas/13_apis_rest.md)
- [14 - Introdução ao Nodejs com TypeScript](./Aulas/14_introducao_nodejs_typescript.md)
- [15 - Avaliação 3](./Aulas/15_avaliacao_3.md)
- [16 - Desafio Final](./Aulas/16_desafio_final.md)